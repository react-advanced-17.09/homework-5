import axios from 'axios';
import { calculateScore, sortPlayers } from '../helpers'

const id = 'YOUR_CLIENT_ID';
const sec = 'YOUR_SECRET_ID';
const params = '?client_id=' + id + '?client_secret=' + sec;

export type Repo = {
    incomplete_results: boolean;
    items: Array<any>;
    total_count: number;
};

type LenguageResponse = {
    data: Repo[];
};

export type ResultResponse = {
    profile: ProfileResponse; 
    score: number 
}

export type ProfileResponse = {
    followers: number;
    score: number;
}

const baseSongConfig = axios.create({
    baseURL: 'http://api.github.com',
    headers: {
        'Content-Type': 'application/json'
    },
});

const errorRequest = (error) => {
    if (axios.isAxiosError(error)) {
        console.log('Error Reading data: ', error.message);
        throw new Error(error.message)
    } else {
        console.log('An unexpected error occurred');
        throw new Error(error.message)
    }
}


export async function getReposByLanguage(language: string): Promise<LenguageResponse>{
    try {
        const { data } = await baseSongConfig.get<LenguageResponse>(`/search/repositories?q=stars:>1+language:${language}&sort=stars&order=desc&type=Repositories`)
        console.log('data', data)
        return data;
    } catch (error) {
        errorRequest(error);
    }
}

export async function getProfile(userName: string): Promise<ProfileResponse> {
    try {
        const { data } = await baseSongConfig.get<ProfileResponse>(`users/${userName}${params}`)
        console.log('data getProfile', data)
        return data;
    } catch (error) {
        errorRequest(error);
    }
}

export async function getRepos(userName: string) {
    try {
        const { data } = await baseSongConfig.get(`users/${userName}/repos${params}`)
        return data;
    } catch (error) {
        errorRequest(error);
    }
}

export function getUserData(userName: string): Promise<ResultResponse> {
    return Promise.all([
        getProfile(userName),
        getRepos(userName)
    ]).then(([profile, repos]) => {
        return {
            profile,
            score: calculateScore(profile, repos)
        }
    })
}

export function startBattle(players: Array<string>): Promise<ResultResponse[]> {
    return Promise.all(players.map(getUserData))
        .then(sortPlayers)
        .catch(errorRequest);
}
