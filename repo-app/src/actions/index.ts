import { AnyAction } from "redux"

export const reposActions = {
    LOAD_REPOS: '[REPOS] Load Repos',
    SET_FIRST_PLAYER_DATA: '[SET_FIRST_PLAYER_DATA] Set first player data',
    SET_SECOND_PLAYER_DATA: '[SET_SECOND_PLAYER_DATA] Set second player data',
    SET_PLAYERS_RESULT: '[SET_PLAYERS_RESULT] Set players result',
}

export const loadRepos = (repos): AnyAction => ({
    type: reposActions.LOAD_REPOS,
    payload: repos
})

export const setFirstPlayersData = (player): AnyAction => ({
    type: reposActions.SET_FIRST_PLAYER_DATA,
    payload: player
})

export const setSecondPlayersData = (player): AnyAction => ({
    type: reposActions.SET_SECOND_PLAYER_DATA,
    payload: player
})

export const setPlayersResult = (players): AnyAction => ({
    type: reposActions.SET_PLAYERS_RESULT,
    payload: players
})