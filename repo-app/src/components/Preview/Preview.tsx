import React, { FC } from "react";
import "./Preview.scss";

type PreviewProps = {
  previewImg: string,
  title: string,
  children?: JSX.Element
}

const Preview: FC<PreviewProps> = ({ previewImg, title, children }) => {
  console.log(typeof(children), 'children')
  return (
    <div className="preview-card">
      <img className="preview-img" src={`http://github.com/${previewImg}.png?size200`} alt="Avatart" />
      <span className="heading-sm">{title}</span>
      {children}
    </div>
  );
};

export default Preview;
