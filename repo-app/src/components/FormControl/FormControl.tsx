import React, { FC } from "react";
import { useState } from "react";
import "./FormControl.scss";

type FormControlProps = {
  label: string,
  plaseholder: string,
  id: string,
  onSubmit: Function
}

const FormControl: FC<FormControlProps> = ({ label, plaseholder, id, onSubmit }) => {
  const [userName, setUserName] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    onSubmit(id, userName);
  };
  
  return (
    <form className={"form"} onSubmit={handleSubmit}>
      <label className="form-label" htmlFor={id}>
        {label}
      </label>
      <input
        id={id}
        className="form-control"
        type="text"
        placeholder={plaseholder}
        value={userName}
        onChange={(event) => setUserName(event.target.value)}
        autoComplete="off"
      />
      <input
        className="btn"
        type="submit"
        disabled={!userName}
        value={"Submit"}
      />
    </form>
  );
};

export default FormControl;
