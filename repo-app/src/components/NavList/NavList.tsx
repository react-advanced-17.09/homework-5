import React, { FC } from "react";
import { NavLink } from "react-router-dom";
import "./NavList.scss";

const navItems = [
  {
    link: 'Home',
    route: '/'
  },
  {
    link: 'Popular',
    route: '/popular'
  },
  {
    link: 'Battle',
    route: '/battle'
  }
]

const NavList: FC = () => {
  return (
    <nav className="nav">
      <ul className="nav-list">
        {navItems.map((navItem, index) => {
          return (
            <li className="nav-item" key={index}>
              <NavLink end to={navItem.route}>
                {navItem.link}
              </NavLink>
            </li>
          )
        })}
      </ul>
    </nav>
  );
}

export default NavList;
