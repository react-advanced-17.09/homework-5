import React, { FC } from "react";
import "./NoData.scss";

const NoData: FC = () => {
  return (
    <div className="error-page">
      <h1 className="error-title">No Data to display ...</h1>
    </div>
  );
}

export default NoData;
