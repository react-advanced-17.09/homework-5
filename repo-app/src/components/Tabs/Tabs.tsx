import React, { FC } from "react";
import {
  useNavigate,
  createSearchParams,
} from 'react-router-dom';
import "./Tabs.scss";

const langs = ["All", "Javascript", "Ruby", "CSS", "Python", "Java"];

type TabsProps = {
  onSelectedLanguage: Function,
  language: string,
}

const Tabs: FC<TabsProps> = ({ onSelectedLanguage, language }) => {
  const navigate = useNavigate();
  
  const setQueryParams = (lang: string) => {
    const params = { language: lang };
    navigate({
      pathname: '/popular',
      search: `?${createSearchParams(params)}`,
    });
  }

  return (
    <ul className="tab-list">
      {langs.map((item, index) => {
        return (
          <li
            className={item === language ? "tab-item is-active" : "tab-item"}
            onClick={() => {
              onSelectedLanguage(item)
              setQueryParams(item)
            }}
            key={index}
          >
            {item}
          </li>
        );
      })}
    </ul>
  );
};

export default Tabs;
