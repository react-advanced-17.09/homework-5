import React, { FC } from "react";
import "./Card.scss";

type CardProps = {
  data: {
    owner: {
      avatar_url: string,
      login: string,
    }
    html_url: string,
    name: string,
    stargazers_count: number
  },
}

const Card: FC<CardProps> = ({data}) => {
  return (
    <div className="card">
        <div className="card-media">
            <img className="card-media-content" src={data.owner.avatar_url} alt="avatar" />
        </div>
        <a className="card-link" href={data.html_url}>{data.name}</a>
        <span className="card-text">{data.owner.login}</span>
        <span className="card-text">{data.stargazers_count}</span>
    </div>
  );
}

export default Card;
