import { AnyAction } from "redux";
import { reposActions } from "../actions/index";

interface IinitialState {
    repos: Array<object>,
    playerOne: string,
    playerTwo: string,
    battleResult: object
}

export const initialState: IinitialState = {
    repos: [],
    playerOne: "",
    playerTwo: "",
    battleResult: {}
}

export const reposReducer = (state: IinitialState = initialState, action: AnyAction) => {
    switch (action.type) {
        case reposActions.LOAD_REPOS: {
            return {
                ...state,
                repos: action.payload
            }
        }
        case reposActions.SET_FIRST_PLAYER_DATA: {
            return {
                ...state,
                playerOne: action.payload
            }
        }
        case reposActions.SET_SECOND_PLAYER_DATA: {
            return {
                ...state,
                playerTwo: action.payload
            }
        }
        case reposActions.SET_PLAYERS_RESULT: {
            console.log(action.payload, 'action.payload')
            return {
                ...state,
                battleResult: action.payload
            }
        }
        default:
            return state;
    }
}