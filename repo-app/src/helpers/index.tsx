import { ResultResponse } from "../api";

export const getStarCount = (repos: Array<object>) => {
    return repos.reduce((acc, repo: {stargazers_count: number}) => {
        return acc + repo.stargazers_count;
    }, 0)
}

export const calculateScore = (profile: { followers: number; }, repos: Array<object>): number => {
    const followers: number = profile.followers;
    const totalStars: number = getStarCount(repos);
    return followers + totalStars;
}

export const sortPlayers = (players: ResultResponse[]) => {
    return players.sort((a: {score: number}, b: {score: number}) =>
        b.score - a.score
    )
}
