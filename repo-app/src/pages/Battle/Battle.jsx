import React from "react";
import { Link, useLocation } from "react-router-dom";
import FormControl from "../../components/FormControl/FormControl";
import Preview from "../../components/Preview/Preview";
import { useSelector, useDispatch } from "react-redux";
import { setFirstPlayersData, setSecondPlayersData } from "../../actions";

const Battle = () => {
  const players = useSelector((state) => state);
  const dispatch = useDispatch();

  const location = useLocation();

  const handleSubmit = (id, userName) => {
    if (id === "playerOne") {
      dispatch(setFirstPlayersData(userName));
    } else {
      dispatch(setSecondPlayersData(userName));
    }
  };

  const handleReset = (id) => {
    if (id === "playerOne") {
      dispatch(setFirstPlayersData(""));
    } else {
      dispatch(setSecondPlayersData(""));
    }
  };

  return (
    <div className="flex flex-column flex-align-center">
      <div className="flex w-100 mb-15">
        <div className="flex-auto">
          {!players.playerOne ? (
            <FormControl
              label={"Player one"}
              id={"playerOne"}
              plaseholder={"GitHub username"}
              onSubmit={handleSubmit}
            />
          ) : (
            <Preview
              previewImg={players.playerOne}
              title={players.playerOne}
              id={"playerOne"}
            >
              <button className="btn" onClick={() => handleReset("playerOne")}>
                Reset
              </button>
            </Preview>
          )}
        </div>
        <div className="flex-auto">
          {!players.playerTwo ? (
            <FormControl
              label={"Player Two"}
              id={"playerTwo"}
              plaseholder={"GitHub username"}
              onSubmit={handleSubmit}
            />
          ) : (
            <Preview
              previewImg={players.playerTwo}
              title={players.playerTwo}
              id={"playerTwo"}
            >
              <button className="btn" onClick={() => handleReset("playerTwo")}>
                Reset
              </button>
            </Preview>
          )}
        </div>
      </div>
      {players.playerOne && players.playerTwo && (
        <Link
          className="btn-link"
          to={{
            pathname: `${location.pathname}/results`,
            search: `?playerOneName=${players.playerOne}&playerTwoName=${players.playerTwo}`,
          }}
        >
          Battle
        </Link>
      )}
    </div>
  );
};

export default Battle;
