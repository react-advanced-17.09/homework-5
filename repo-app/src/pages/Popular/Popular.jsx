import React, { useState, useEffect, useCallback } from "react";
import {
  useSearchParams
} from 'react-router-dom';
import { useSelector, useDispatch } from "react-redux";
import Tabs from "../../components/Tabs/Tabs";
import { getReposByLanguage } from "../../api";
import Card from "../../components/Card/Card";
import Loader from "../../components/Loader/Loader";
import { debounce } from "debounce";
import { loadRepos } from "../../actions";


const Popular = () => {
  const [paramsLanguage] = useSearchParams();
  const [language, setLanguage] = useState(paramsLanguage.get("language") || 'All');
  const [loading, setLoading] = useState(false);

  const allRepos = useSelector((state) => state.repos);

  const dispatch = useDispatch();

  async function fetchLangRepos(lang) {
    setLoading(true);
    const res = await getReposByLanguage(lang);
    dispatch(loadRepos(res.items))
    setLoading(false);
  }

  const debouncedFetch = useCallback(debounce(fetchLangRepos, 500), []);

  useEffect(() => {
    debouncedFetch(language)
  }, [language]);

  return (
    <>
      <Tabs language={language} onSelectedLanguage={setLanguage}></Tabs>
      <div className="flex flex-wrap">
        {loading ? (
          <Loader />
          ) : (
            allRepos.map((repo) => <Card key={repo.id} data={repo} />)
        )}
      </div>
    </>
  );
};

export default Popular;
