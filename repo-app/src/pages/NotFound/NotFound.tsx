import React, { FC } from "react";
import "./NotFound.scss";

const NotFound: FC = () => {
  return (
    <div className="error-page">
      <h1 className="error-title">Sorry ...</h1>
    </div>
  );
}

export default NotFound;
