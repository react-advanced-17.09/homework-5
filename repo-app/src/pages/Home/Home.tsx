import React, { FC } from "react";
import { Link } from "react-router-dom";

const Home: FC = () => {
  return (
    <div className="flex flex-column flex-align-center">
      <span className="heading-md">
        Github Battle: Battle your friends .... and staff
      </span>
      <Link to="battle" className="btn-link">
        {" "}
        Battle now
      </Link>
    </div>
  );
};

export default Home;
