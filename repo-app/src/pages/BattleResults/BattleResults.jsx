import React, { useState } from "react";
import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import { startBattle } from "../../api";
import { useSelector, useDispatch } from "react-redux";
import Loader from "../../components/Loader/Loader";
import Preview from "../../components/Preview/Preview";
import NoData from "../../components/NoData/NoData";
import { setPlayersResult } from "../../actions";


const BattleResults = () => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const dispatch = useDispatch();
  const battleResult = useSelector((state) => state.battleResult);

  const location = useLocation();

  useEffect(() => {
    
    const searchParams = new URLSearchParams(location.search);
    const playerOneParams = searchParams.get("playerOneName");
    const playerTwoParams = searchParams.get("playerTwoName");

    startBattle([playerOneParams, playerTwoParams]).then(
      ([winner, loser]) => {
        if (!winner && !loser) {
          setError(true);
          return;
        }

        dispatch(setPlayersResult({winner, loser}));

        setLoading(false);
      }
    );
  }, []);

  return (
    <div className="flex flex-column flex-align-center">
      {loading ? (
        <Loader />
      ) : (
        <div className="flex w-100 mb-15">
          {error ? (
            <NoData />
          ) : (
            <>
              <div className="flex-auto text-center">
                <span className="heading-sm mb-15">Winner</span>
                <Preview
                  previewImg={battleResult.winner?.profile.login}
                  title={battleResult.winner?.profile.login}
                >
                  <span>{battleResult.winner.score}</span>
                </Preview>
              </div>
              <div className="flex-auto text-center">
                <span className="heading-sm mb-15">Loser</span>
                <Preview
                  previewImg={battleResult.loser.profile.login}
                  title={battleResult.loser.profile.login}
                >
                  <span>{battleResult.loser.score}</span>
                </Preview>
              </div>
            </>
          )}
        </div>
      )}
    </div>
  );
};

export default BattleResults;
