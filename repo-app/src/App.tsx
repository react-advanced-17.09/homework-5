import React from 'react';
import './App.scss';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import NavList from './components/NavList/NavList';
import BattleResults from './pages/BattleResults/BattleResults';
import Home from './pages/Home/Home';
import Popular from './pages/Popular/Popular';
import Battle from './pages/Battle/Battle';
import NotFound from './pages/NotFound/NotFound';


function App() {
  return (
    <BrowserRouter>
        <NavList/>
        <div className='container'>
          <Routes>
            <Route index element={<Home />} />
            <Route path="/popular" element={<Popular />} />
            <Route path="/battle" element={<Battle />} />
            <Route path="/battle/results" element={<BattleResults />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </div>
    </BrowserRouter>
  );
}

export default App;
