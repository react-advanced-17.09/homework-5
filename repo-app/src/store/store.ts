import { createStore, applyMiddleware, AnyAction } from 'redux';
import { reposReducer } from '../reducers/index';
import logger from 'redux-logger';
import thunk, { ThunkAction } from 'redux-thunk';


export type RootState = ReturnType<typeof store.getState>
export type AddDispatch = typeof store.dispatch
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, AnyAction>

export const store = createStore(reposReducer, applyMiddleware(logger));